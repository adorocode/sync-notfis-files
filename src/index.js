const cron = require("node-cron")
const fs = require("fs")
var shell = require('shelljs');

const {timeInterval, filePath} = JSON.parse(fs.readFileSync("config.json", {encoding: 'utf8'}))

function runFile(filePath){
    shell.exec(`call ${filePath}`)
}

runFile(filePath)

cron.schedule(`*/${timeInterval} * * * *`, () => {
    console.log(new Date(), "Iniciando envio do notfis...")
    runFile(filePath)
});
